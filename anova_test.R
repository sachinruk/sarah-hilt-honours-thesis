source('~/Documents/Sarah/organicFrequency.R')
source('~/Documents/Sarah/readTable.R')
source('~/Documents/Sarah/readTable2.R')
setwd('~/Documents/Sarah/')
rawData<-readTable()
rawData2<-readTable2()
#grab the data that we are concerned about
data<-data.frame(rawData$Vals_SQ001,rawData$Vals_SQ009,rawData$OFB1)
#data2<-data.frame(rawData2$Vals_SQ001,rawData2$Vals_SQ009,rawData2$OFB1)
names(data)<-c("Equality","Freedom","frequency")
data$Freedom<-as.numeric(data$Freedom)
data$Equality<-as.numeric(data$Equality)
data<-organicFrequency(data)

boxplot(data$Equality~data$freqCategory)
Equality.aov<-aov(data$Equality~factor(data$freqCategory))
#fm1=lme(data$Equality~data$freqCategory)
summary(Equality.aov)
TukeyHSD(Equality.aov)

boxplot(data$Freedom~data$freqCategory)
Freedom.aov<-aov(data$Freedom~factor(data$freqCategory))
#fm1=lme(data$Equality~data$freqCategory)
summary(Freedom.aov)
TukeyHSD(Freedom.aov)


#percentage of high intakers
HIpercentage=sum(data$freqCategory=='H')/length(data$freqCategory=='H')

#boxplot(as.character(data$frequency)~data$Equality)
#data$freqCat<-organicFrequency(data$frequency
#org                              